package registry

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/aubeantoine/inventions-registry/inventions"
	"io/ioutil"
	"os"
)

type fileInventionsBase struct {
	filePath string
}

func (base *fileInventionsBase) List() ([]inventions.Invention, error) {
	jsonFile, err := os.Open(base.filePath)
	if err != nil {
		log.WithError(err).Error("failed to open JSON file")
		return nil, err
	}

	defer func() {
		if err := jsonFile.Close(); err != nil {
			log.WithError(err).Error("failed to close JSON file")
		}
	}()

	jsonBytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.WithError(err).Error("failed to read bytes of JSON file")
		return nil, err
	}

	var found []inventions.Invention
	if err := json.Unmarshal(jsonBytes, &found); err != nil {
		log.WithError(err).Error("failed to translate bytes into inventions")
		return nil, err
	}

	return found, nil
}

func NewFileInventionsBase(filePath string) InventionsBase {
	return &fileInventionsBase{filePath: filePath}
}
