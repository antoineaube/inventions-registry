package registry

import "gitlab.com/aubeantoine/inventions-registry/inventions"

type InventionsBase interface {
	List() ([]inventions.Invention, error)
}
