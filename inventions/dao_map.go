package inventions

import (
	"errors"
	"sort"
	"strings"
)

type mapDao struct {
	lastIndex         int
	contentInventions map[InventionID]Invention
	taggedInventions  map[string][]InventionID
}

func (dao *mapDao) mapInventionsIDsToTagOccurrence(tags ...string) map[InventionID]int {
	inventionsOccurrences := map[InventionID]int{}
	for _, tag := range tags {
		inventionsOfTags := dao.taggedInventions[tag]

		for _, taggedID := range inventionsOfTags {
			currentOccurrences, ok := inventionsOccurrences[taggedID]
			if !ok {
				currentOccurrences = 0
			}

			inventionsOccurrences[taggedID] = currentOccurrences + 1
		}
	}

	return inventionsOccurrences
}

func (dao *mapDao) ReadMostSimilar(ID InventionID) (Invention, error) {
	mainInvention, err := dao.ReadOne(ID)
	if err != nil {
		return Invention{}, err
	}

	inventionsOccurrences := dao.mapInventionsIDsToTagOccurrence(mainInvention.Tags...)
	delete(inventionsOccurrences, ID)

	currentOccurrences := 0
	current := InventionID(-1)
	for inventionID, occurrences := range inventionsOccurrences {
		if (occurrences > currentOccurrences) || (dao.contentInventions[current].Year > dao.contentInventions[inventionID].Year) {
			current = inventionID
			currentOccurrences = occurrences
			continue
		}
	}

	if current == InventionID(-1) {
		return Invention{}, errors.New("no similar invention found")
	}

	return dao.ReadOne(current)
}

func (dao *mapDao) ReadAllWithTag(tag string) ([]Invention, error) {
	associatedIDs, ok := dao.taggedInventions[tag]

	if !ok {
		// The tag does not exist.
		return []Invention{}, nil
	}

	associatedInventions := make([]Invention, len(associatedIDs))
	for i, inventionID := range associatedIDs {
		associatedInventions[i] = dao.contentInventions[inventionID]
	}

	sort.Slice(associatedInventions, func(i, j int) bool {
		return associatedInventions[i].ID < associatedInventions[j].ID
	})

	return associatedInventions, nil
}

func (dao *mapDao) DeleteAll() error {
	dao.lastIndex = 0
	dao.contentInventions = map[InventionID]Invention{}
	dao.taggedInventions = map[string][]InventionID{}

	return nil
}

func (dao *mapDao) DeleteOne(ID InventionID) error {
	invention, ok := dao.contentInventions[ID]
	if !ok {
		return errors.New("invention not found")
	}

	delete(dao.contentInventions, ID)

	for _, tag := range invention.Tags {
		dao.removeTag(tag, ID)
	}

	return nil
}

func (dao *mapDao) ReadAll() ([]Invention, error) {
	if len(dao.contentInventions) == 0 {
		return []Invention{}, nil
	}

	var inventions []Invention
	for _, storedInvention := range dao.contentInventions {
		inventions = append(inventions, storedInvention)
	}

	sort.Slice(inventions, func(i, j int) bool {
		if inventions[i].Year == inventions[j].Year {
			return strings.ToLower(inventions[i].Name) < strings.ToLower(inventions[j].Name)
		}

		return inventions[i].Year < inventions[j].Year
	})

	return inventions, nil
}

func (dao *mapDao) ReadOne(ID InventionID) (Invention, error) {
	found := dao.contentInventions[ID]

	if found.ID != ID {
		return found, errors.New("invention not found")
	}

	return found, nil
}

func (dao *mapDao) Create(invention Invention) (InventionID, error) {
	dao.lastIndex++

	createdInvention := &invention
	createdInvention.ID = InventionID(dao.lastIndex)

	dao.contentInventions[createdInvention.ID] = *createdInvention

	for _, tag := range invention.Tags {
		dao.addTag(tag, createdInvention.ID)
	}

	return createdInvention.ID, nil
}

func (dao *mapDao) addTag(tag string, ID InventionID) {
	alreadyExisting, ok := dao.taggedInventions[tag]
	if !ok {
		alreadyExisting = []InventionID{}
	}

	dao.taggedInventions[tag] = append(alreadyExisting, ID)
}

func (dao *mapDao) removeTag(tag string, ID InventionID) {
	existingIDs, ok := dao.taggedInventions[tag]

	if !ok {
		return
	}

	for i, existingID := range existingIDs {
		if existingID == ID {
			existingIDs[i] = existingIDs[len(existingIDs)-1]
			existingIDs[len(existingIDs)-1] = InventionID(0)
			existingIDs = existingIDs[:len(existingIDs)-1]

			dao.taggedInventions[tag] = existingIDs
		}
	}
}

func NewMapDao() Dao {
	return &mapDao{
		lastIndex:         0,
		contentInventions: map[InventionID]Invention{},
		taggedInventions:  map[string][]InventionID{},
	}
}
