package inventions

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMapDao_CreateTwoInventions(t *testing.T) {
	dao := NewMapDao()

	createdID, err := dao.Create(Invention{
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	})

	assert.Equal(t, InventionID(1), createdID)
	assert.Nil(t, err)

	createdID, err = dao.Create(Invention{
		Year:     1896,
		Name:     "L'invention du tube de dentifrice",
		Inventor: "Colgate & Company",
		Origin:   "Royaume-Uni",
	})

	assert.Equal(t, InventionID(2), createdID)
	assert.Nil(t, err)

	storedInventions := dao.(*mapDao).contentInventions

	assert.Len(t, storedInventions, 2)

	assert.Equal(t, Invention{
		ID:       InventionID(1),
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	}, storedInventions[InventionID(1)])

	assert.Equal(t, Invention{
		ID:       InventionID(2),
		Year:     1896,
		Name:     "L'invention du tube de dentifrice",
		Inventor: "Colgate & Company",
		Origin:   "Royaume-Uni",
	}, storedInventions[InventionID(2)])
}

func TestMapDao_ReadAll(t *testing.T) {
	dao := NewMapDao()

	dao.(*mapDao).contentInventions = map[InventionID]Invention{
		InventionID(1): {
			ID:       InventionID(1),
			Year:     1974,
			Name:     "L'invention de la carte à puce",
			Inventor: "Roland Moreno",
			Origin:   "France",
			Site:     "",
			Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
		},
		InventionID(2): {
			ID:       InventionID(2),
			Year:     1896,
			Name:     "L'invention du tube de dentifrice",
			Inventor: "Colgate & Company",
			Origin:   "Royaume-Uni",
		},
	}

	storedInventions, err := dao.ReadAll()

	assert.Nil(t, err)
	assert.Len(t, storedInventions, 2)
	assert.Equal(t, Invention{
		ID:       2,
		Year:     1896,
		Name:     "L'invention du tube de dentifrice",
		Inventor: "Colgate & Company",
		Origin:   "Royaume-Uni",
	}, storedInventions[0])
	assert.Equal(t, Invention{
		ID:       1,
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	}, storedInventions[1])
}

func TestMapDao_ReadAllWithSorting(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year: -200,
		Name: "Another very old invention",
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	})
	assert.Nil(t, err)

	storedInventions, err := dao.ReadAll()

	assert.Nil(t, err)
	assert.Len(t, storedInventions, 4)
	assert.Equal(t, Invention{
		ID:   3,
		Year: -200,
		Name: "Another very old invention",
	}, storedInventions[0])
	assert.Equal(t, Invention{
		ID:   2,
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	}, storedInventions[1])
	assert.Equal(t, Invention{
		ID:       4,
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	}, storedInventions[2])
	assert.Equal(t, Invention{
		ID:       1,
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	}, storedInventions[3])
}

func TestMapDao_ReadAllWhenEmpty(t *testing.T) {
	dao := NewMapDao()

	found, err := dao.ReadAll()

	assert.Nil(t, err)
	assert.NotNil(t, found)
	assert.Len(t, found, 0)
}

func TestMapDao_ReadOne(t *testing.T) {
	dao := NewMapDao()

	inserted := Invention{
		ID:       InventionID(1),
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	}

	dao.(*mapDao).contentInventions[InventionID(1)] = inserted

	found, err := dao.ReadOne(InventionID(1))

	assert.Nil(t, err)
	assert.Equal(t, inserted, found)
}

func TestMapDao_ReadOneFailingIfNotExist(t *testing.T) {
	dao := NewMapDao()

	found, err := dao.ReadOne(InventionID(1))

	assert.NotNil(t, err)
	assert.Equal(t, Invention{}, found)
}

func TestMapDao_DeleteOne(t *testing.T) {
	dao := NewMapDao()

	inserted := Invention{
		ID:       1,
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	}

	dao.(*mapDao).contentInventions[1] = inserted

	assert.Nil(t, dao.DeleteOne(1))

	_, ok := dao.(*mapDao).contentInventions[1]
	assert.False(t, ok)
}

func TestMapDao_CreateAfterDeletion(t *testing.T) {
	dao := NewMapDao()

	inserted := Invention{
		ID:       1,
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	}

	_, _ = dao.Create(inserted)

	assert.Nil(t, dao.DeleteOne(1))

	createdID, err := dao.Create(inserted)

	assert.Equal(t, InventionID(2), createdID)
	assert.Nil(t, err)
}

func TestMapDao_DeleteOneFailIfNotExists(t *testing.T) {
	dao := NewMapDao()

	err := dao.DeleteOne(1)

	assert.NotNil(t, err)
	assert.Equal(t, "invention not found", err.Error())
}

func TestMapDao_ReadAllWithTag(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	})
	assert.Nil(t, err)

	found, err := dao.ReadAllWithTag("Feuille")

	assert.Nil(t, err)

	assert.Len(t, found, 2)
	assert.Equal(t, Invention{
		ID:   2,
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	}, found[0])
	assert.Equal(t, Invention{
		ID:       3,
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	}, found[1])
}

func TestMapDao_ReadAllWithTagWhenNoAssociatedInvention(t *testing.T) {
	dao := NewMapDao()

	found, err := dao.ReadAllWithTag("abc")

	assert.Nil(t, err)
	assert.NotNil(t, found)
	assert.Len(t, found, 0)
}

func TestMapDao_DeleteAll(t *testing.T) {
	dao := NewMapDao()

	inserted := Invention{
		ID:       1,
		Year:     1974,
		Name:     "L'invention de la carte à puce",
		Inventor: "Roland Moreno",
		Origin:   "France",
		Site:     "",
		Tags:     []string{"Electronique", "Paiement", "Argent", "Banque", "Data", "Plastique"},
	}

	dao.(*mapDao).contentInventions[1] = inserted

	assert.Nil(t, dao.DeleteAll())
	assert.Empty(t, dao.(*mapDao).contentInventions)

	createdID, err := dao.Create(inserted)
	assert.Equal(t, InventionID(1), createdID) // ID are reset
	assert.Nil(t, err)
}

func TestMapDao_ReadAllWithTagAfterDeletion(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	})
	assert.Nil(t, err)

	assert.Nil(t, dao.DeleteOne(2)) // Delete "L'invention du papier"

	found, err := dao.ReadAllWithTag("Feuille")

	assert.Nil(t, err)

	assert.Len(t, found, 1)
	assert.Equal(t, Invention{
		ID:       3,
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	}, found[0])

	assert.Nil(t, dao.DeleteOne(3)) // Delete "L'invention du journal"

	found, err = dao.ReadAllWithTag("Feuille")

	assert.Nil(t, err)

	assert.Len(t, found, 0)
}

func TestMapDao_ReadAllWithTagAfterDeleteAll(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	})
	assert.Nil(t, err)

	assert.Nil(t, dao.DeleteAll())

	found, err := dao.ReadAllWithTag("Papier")

	assert.Nil(t, err)

	assert.Len(t, found, 0)
}

func TestMapDao_ReadMostSimilar(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year: 1799,
		Name: "L'adoption du système métrique en France",
		Tags: []string{"Taille", "Dimention"},
	})
	assert.Nil(t, err)
	_, err = dao.Create(Invention{
		Year: 1730,
		Name: "L'invention du sextant",
		Tags: []string{"Etoile", "Lumiere"},
	})
	assert.Nil(t, err)
	_, err = dao.Create(Invention{
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	})
	assert.Nil(t, err)
	_, err = dao.Create(Invention{
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	})
	assert.Nil(t, err)

	found, err := dao.ReadMostSimilar(4)

	assert.Nil(t, err)
	assert.Equal(t, Invention{
		ID:       3,
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	}, found)
}

func TestMapDao_ReadMostSimilarOfUnknown(t *testing.T) {
	dao := NewMapDao()
	_, err := dao.ReadMostSimilar(1)

	assert.NotNil(t, err)
}

func TestMapDao_ReadMostSimilarKeepOldest(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	})
	assert.Nil(t, err)

	_, err = dao.Create(Invention{
		Year:     1605,
		Name:     "L'invention du journal",
		Inventor: "Johann Carolus",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur", "Feuille"},
	})
	assert.Nil(t, err)

	found, err := dao.ReadMostSimilar(1)

	assert.Nil(t, err)
	assert.Equal(t, Invention{
		ID:   2,
		Year: -200,
		Name: "L'invention du papier",
		Tags: []string{"Papier", "Ecriture", "Feuille"},
	}, found)
}

func TestMapDao_ReadMostSimilarNotFound(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.ReadMostSimilar(0)
	assert.NotNil(t, err)
	assert.NotEqual(t, "no similar invention found", err)
}

func TestMapDao_ReadMostSimilarNothingSimilar(t *testing.T) {
	dao := NewMapDao()

	_, err := dao.Create(Invention{
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	})
	assert.Nil(t, err)

	_, err = dao.ReadMostSimilar(0)
	assert.NotNil(t, err)
	assert.Equal(t, "no similar invention found", err.Error())
}
